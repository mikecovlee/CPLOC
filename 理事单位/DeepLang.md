## 项目基本信息
1. 名称：DeepLang
2. 官网：https://deeplang.org/
3. 仓库：https://github.com/deeplang-org/deeplang
4. 简介（200字以内）：我们致力于将DeepLang语言打造为具有鲜明内存安全特性的面向IoT场景的语言，设计过程中参考Rust的安全机制，但又根据IoT场景的特性选择了更合适的解释执行模式。DeepLang是一种静态类型、强类型语言，参考C-style设计语法，同时支持过程式、逻辑式和函数式的混合范式。

## 理事单位代表信息
5. 理事代表：杨海龙
6. 联系方式（邮箱、电话、即时通讯工具账号等均可）：swubear@163.com
7. Gitee账号：chinesebear

备注：第2、3项至少填一项；第6、7项至少填一项，为便于提案投票，尽可能提供第7项。
